## Validierung verschiedener NER-Verfahren in Python

Dieses Repository enthält den Programmcode und den Datensatz, die in der Studie "Validierung von NER-Verfahren zur automatisierten Identifikation von Akteur:innen in deutschsprachigen journalistischen Texten" verwendet wurden. Die Studie wurde in [*Studies in Communication|Media (SCM)* publiziert](https://doi.org/10.5771/2192-4007-2021-4-590).

Die Studie befasst sich mit der Validierung von Named Entity Recognition (NER), einem Verfahren, das als Teilschritt der Inhaltsanalyse von umfangreichen Textdaten eingesetzt werden kann und auf die automatisierte Identifikation und Extraktion von Eigennamen (Personen, Organisationen, Orte) in Texten spezialisiert ist.
Für diesen Zweck werden oft frei verfügbare NER-Softwarepakete verwendet, die mit spezifischen Textdaten trainiert und optimiert wurden. Dadurch ist aber ungewiss, ob diese NER-Pakete bei der Analyse von unbekannten journalistischen Nachrichtentexten richtige und präzise Ergebnisse liefern können.
Um dies zu evaluieren, wurden drei in der Programmiersprache Python implementierte NER-Codepackages gegenübergestellt: [spacy](https://spacy.io/), [stanza](https://stanfordnlp.github.io/stanza/) und [flair](https://github.com/flairNLP/flair).
Die Ergebnisse der automatisierten Analyse wurden mit den Ergebnissen einer manuellen Inhaltsanalyse derselben journalistischen Textdaten verglichen.    
Ziel war damit, die Eignung und Güte verschiedener NER-Softwarepakete für die Identifikation von Akteur:innen zu prüfen, denn obwohl in der Kommunikationswissenschaft vermehrt automatisierte Verfahren eingesetzt werden, mangelt es an Studien, die die Validität der erhaltenen Ergebnisse bewerten.
Die Ergebnisse zeigen eine hohe Übereinstimmung zwischen den händisch erhobenen und den automatisiert identifizierten Personennamen, lediglich bei der automatisierten Identifikation von Organisationsnamen ist die Übereinstimmungsquote mit den manuellen Codierungen geringer. 

Der Code zur Vorverarbeitung der Texte sowie zur Auswertung der erkannten Eigennamen bei allen drei Paketen ist im Jupyter-Notebook *ner_pipelines.ipynb* dokumentiert. Der in dieser Analyse erzeugte Datensatz findet sich im Ordner *data* als csv-Datei.
Die Datei enthält je Artikel die Quelle (Spalte *source*), den Titel (*title*) sowie je verwendetes Package die erkannten Eigennamen jeder Kategorie (bspw. *spacy_persons*, *spacy_misc*, *stanza_organisations* oder *flair_locations*).
In diesen Spalten sind die verschiedenen erkannten Eigennamen mit Semikolons unterteilt, zum Aufteilen kann man in Python in Pandas-Dataframes bspw. folgende Codezeile verwenden:    
`all_packages_all_entities["spacy_persons_separiert"] = all_packages_all_entities["spacy_persons"].apply(lambda x: x.split("; "))`